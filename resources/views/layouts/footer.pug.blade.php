.container
	.row
		.col-sm-3
			h4.title-widget About us
			p
				| Đây là dự án phục vụ học tập của nhóm 1 trong bài Assignment môn PHP 3, toàn bộ nội dung chỉ mang tính chất tham khảo.
			ul.social.social-circle
				li
					a.icoFacebook(href='#')
						i.fa.fa-facebook
				li
					a.icoTwitter(href='#')
						i.fa.fa-twitter
				li
					a.icoGoogle(href='#')
						i.fa.fa-google-plus
				li
					a.icoRss(href='#')
						i.fa.fa-youtube
		.col-sm-3
			h4.title-widget My Account
			span.acount-icon
				a(href='#')
					i.fa.fa-heart(aria-hidden='true')
					|  Wish List
				a(href='#')
					i.fa.fa-cart-plus(aria-hidden='true')
					|  Cart
				a(href='#')
					i.fa.fa-user(aria-hidden='true')
					|  Profile
				a(href='#')
					i.fa.fa-globe(aria-hidden='true')
					|  Language
		.col-sm-3
			h4.title-widget Category
			.category
				a.zoom(href='#') iphone
				a.zoom(href='#') samsung
				a.zoom(href='#') phu kien
				a.zoom(href='#') tablet
				a.zoom(href='#') cam ung
				a.zoom(href='#') smart phone
				a.zoom(href='#') top
				a.zoom(href='#') promotion
		.col-sm-3
			h4.title Payment Methods
			p Lorem Ipsum is simply dummy text of the printing and typesetting industry.
			ul.payment
				li
					a(href='#')
						i.fa.fa-cc-amex.zoom(aria-hidden='true')
				li
					a(href='#')
						i.fa.fa-credit-card.zoom(aria-hidden='true')
				li
					a(href='#')
						i.fa.fa-paypal.zoom(aria-hidden='true')
				li
					a(href='#')
						i.fa.fa-cc-visa.zoom(aria-hidden='true')
	hr
	.row.text-center  © 2018 By Group 1 - PT12305
