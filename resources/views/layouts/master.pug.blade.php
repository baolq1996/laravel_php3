<!DOCTYPE html>
html(lang="en")
	head
		@include('layouts.head')
		@include('layouts.navbar')
		.container
			.row.my-2
				.col-12.col-md-8
					@include('layouts.header.left')
				.col-12.col-md-4
					@include('layouts.header.right')
	body
		.container
			@section('content')
			@show
	footer.myfooter
		@include('layouts.footer')
		@include('layouts.footerscript')