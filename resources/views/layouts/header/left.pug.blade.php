#carouselExampleIndicators.carousel.slide(data-ride='carousel')
	.carousel-inner(role='listbox')
		.carousel-item.active
			img.d-block.img-fluid(src='https://cdn2.tgdd.vn/qcao/30_12_2017_10_59_38_Oppo-F5-800-300.png', alt='First slide')
		.carousel-item
			img.d-block.img-fluid(src='https://cdn4.tgdd.vn/qcao/19_01_2018_10_19_24_dai-tiec-online-800-300.png', alt='Second slide')
		.carousel-item
			img.d-block.img-fluid(src='https://cdn4.tgdd.vn/qcao/30_10_2017_10_00_40_HC-Tra-Gop-800-300.png', alt='Third slide')
	a.carousel-control-prev(href='#carouselExampleIndicators', role='button', data-slide='prev')
		span.carousel-control-prev-icon(aria-hidden='true')
		span.sr-only Previous
	a.carousel-control-next(href='#carouselExampleIndicators', role='button', data-slide='next')
		span.carousel-control-next-icon(aria-hidden='true')
		span.sr-only Next
	ol.carousel-indicators
		li.active(data-target='#carouselExampleIndicators', data-slide-to='0')
		li(data-target='#carouselExampleIndicators', data-slide-to='1')
		li(data-target='#carouselExampleIndicators', data-slide-to='2')
