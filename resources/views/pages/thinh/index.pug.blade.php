@extends('layouts.master')
@section('title') Home 
@stop
@section('content')
.row
	//Khuyến mãi hot
	.col-12
		.card
			.card-header.bg-gold
				span KHUYẾN MÃI HOT
			.card-block
				.prmt-box.p-3.row
					.col-3
						img.prmt-img(src='https://cdn1.tgdd.vn/Products/Images/42/147146/samsung-galaxy-note-8-orchid-gray-300x300.jpg')
						.prmt-text.mt-2
							div.prmt-title Galaxy Note 8
							div.prmt-price 22.400.000đ
							div Cơ hội 2018 trúng 2018 chỉ vàng
					.col-3
						img.prmt-img(src='https://cdn.tgdd.vn/Products/Images/42/112970/samsung-galaxy-j7-plus-1-300x300.jpg')
						.prmt-text.mt-2
							div.prmt-title Galaxy J7+
							div.prmt-price 8.690.000đ
							div Giảm 1 triệu qua Galaxy Gift
					.col-3
						img.prmt-img(src='https://cdn2.tgdd.vn/Products/Images/42/88537/samsung-galaxy-a7-2017-vangdong-300x300.png')
						.prmt-text.mt-2
							div.prmt-title Samsung Galaxy A7(2017)
							div.d-inline.prmt-price 7.990.000đ
							s.ml-2 9.900.000đ
							div Cơ hội 2018 trúng 2018 chỉ vàng
					.col-3
						img.prmt-img(src='https://cdn.tgdd.vn/Products/Images/42/114115/iphone-x-64gb-1-6-300x300.jpg')
						.prmt-text.mt-2
							div.prmt-title IPhone X 64GB
							div.d-inline.prmt-price 29.990.000đ
							s.ml-2 30.900.000đ
							div Tặng thùng nước ngọt coca 24 lon
	//Điện thoại nổi bật
	.col-12.my-3
		.card
			.card-header.bg-gold
				span ĐIỆN THOẠI NỔI BẬT
			//Nổi bật dòng 1
			.card-block
				.prmt-box.p-3.row
					.col-6
						img.prmt-img(src='https://cdn1.tgdd.vn/Products/Images/42/91131/Feature/samsung-galaxy-s8-plus-10.jpg')
						.prmt-text.mt-2
							div.prmt-title Galaxy S8 Plus
							div.prmt-price 20.490.000đ
							div Cơ hội 2018 trúng 2018 chỉ vàng
					.col-3
						img.prmt-img(src='https://cdn3.tgdd.vn/Products/Images/42/84798/samsung-galaxy-j7-prime-h1-300x300.jpg')
						.prmt-text.mt-2
							div.prmt-title Samsung Galaxy J7 Prime
							div.d-inline.prmt-price 5.490.000đ
							div Tặng phiếu mua hàng 100K
					.col-3
						img.prmt-img(src='https://cdn4.tgdd.vn/Products/Images/42/78479/samsung-galaxy-s8-4-300x300.jpg')
						.prmt-text.mt-2
							div.prmt-title Galaxy S8
							div.d-inline.prmt-price 18.490.000đ
							div Tặng thùng nước ngọt coca 24 lon
			hr
			//Nổi bật dòng 2
			.card-block
				.prmt-box.p-3.row
					.col-3
						img.prmt-img(src='https://cdn.tgdd.vn/Products/Images/42/145295/oppo-a83-h1-300x300.jpg')
						.prmt-text.mt-2
							div.prmt-title OPPO A3
							div.d-inline.prmt-price 4.990.000đ
							div Cơ hội nhận lì xì 1.35 tỷ
					.col-3
						img.prmt-img(src='https://cdn1.tgdd.vn/Products/Images/42/87846/iphone-6s-plus-32gb-vanghong1-h1-300x300.jpg')
						.prmt-text.mt-2
							div.prmt-title iPhone 6s Plus
							div.d-inline.prmt-price 13.490.000đ
							div Tặng thùng nước ngọt coca 24 lon
					.col-6
						img.prmt-img(src='https://cdn.tgdd.vn/Products/Images/42/131915/Feature/oppo-f5-13-1.jpg')
						.prmt-text.mt-2
							div.prmt-title Oppo F5
							div.prmt-price 6.990.000đ
	//Phụ kiện
	.col-12.mb-3
		.card
			.card-header.bg-gold
				span Phụ kiện điện thoại
			.card-block
				.prmt-box.p-3.row
					.col-3
						img.prmt-img(src='https://cdn1.tgdd.vn/Products/Images/57/140801/sac-du-phong-polymer-8700mah-sony-cp-v9-bc-ula-avatar-1-300x300.jpg')
						.prmt-text.mt-2
							div.prmt-title Pin sạc dự phòng Polymer 8700mAh Sony CP-V9/BC ULA
							div.prmt-price 990.000đ
					.col-3
						img.prmt-img(src='https://cdn.tgdd.vn/Products/Images/382/77610/loa-bluetooth-fenda-w5-2-1-300x300.jpg')
						.prmt-text.mt-2
							div.prmt-title Loa Bluetooth Fenda W5
							div.prmt-price 320.000đ
					.col-3
						img.prmt-img(src='https://cdn2.tgdd.vn/Products/Images/58/86507/cap-micro-1m-esaver-ds118br-tb-avatar-300x300.png')
						.prmt-text.mt-2
							div.prmt-title Dây cáp Micro USB 1 m eSaver DS118-TB
							div.d-inline.prmt-price 60.000đ
					.col-3
						img.prmt-img(src='https://cdn3.tgdd.vn/Products/Images/55/69973/the-nho-microsd-16gb-class-10-7-300x300.jpg')
						.prmt-text.mt-2
							div.prmt-title Thẻ nhớ Micro SD 16 GB Class 10
							div.d-inline.prmt-price 195.000đ					
					

@stop