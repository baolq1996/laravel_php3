# Khởi tạo trang
Có thể tạo route riêng hoặc chèn thêm biến như route mặc định tại route/web.php
# Chỉnh sửa trang
Toàn bộ file đã được khởi tạo sẵn trong từng thư mục resources/views/pages/{yourname}/index.pug.blade.php
# Viết nội dung cho trang
Thay thế nội dung trong "section('content')"
Example :
    '@section('content')
    p Viết nội dung ở đây
    @stop'
Ở đoạn code trên thì toàn bộ nội dung trong thẻ body sẽ có dạng <p> Viết nội dung ở đây </p>
# Chỉnh sửa css
Yêu cầu viết bằng scss
Chỉnh sửa theo đường dẫn resources/assets/sass/customize/{yourname}.scss
# Chỉnh sửa js
Chỉnh sửa theo đường dẫn resources/assets/js/customize/{yourname}.js
JS đã bao gồm framework Jquery phiên bản 3.2.1

